# Laravel-CustomUserProvider

Laravel
自作ユーザープロバイダーのサンプルコードです。

## 概要

　独自の認証フローが必要な場合、`Illuminate\Contracts\Auth\UserProvider`の具象UserProviderクラスを作成し、`AuthServiceProvider.php`に登録することで実現可能です。

[こちら](https://gitlab.com/anndou/laravel-customuserprovider/-/compare/70b2b2b9ca892f22957bd9916fc0865603a8f5ba...e106c5445d1d158fec973271b0b0ad2e2c246184)から、必要な変更が確認できます。
