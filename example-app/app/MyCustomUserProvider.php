<?php

namespace App;

use App\Http\Middleware\Authenticate;
use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Http;
use phpDocumentor\Reflection\DocBlock\Tags\Generic;

class MyCustomUserProvider implements Userprovider
{
    public function retrieveById($identifier)
    {
        return new GenericUser([
            'id' => $identifier,
            'email' => $identifier
        ]);
    }

    public function retrieveByToken($identifier, $token)
    {
        return null;
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
    }

    public function retrieveByCredentials(array $credentials)
    {
        if (!array_key_exists('email', $credentials)) {
            return null;
        }

        return new GenericUser([
            'id' => $credentials['email'],
            'email' => $credentials['email']
        ]);
    }

    public function validateCredentials(Authenticatable $user, array $credentials){
        // 無条件にTrueを返すことで、常に認証が成功する
        return true;
    }
}
