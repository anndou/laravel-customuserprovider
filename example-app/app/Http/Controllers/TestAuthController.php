<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class TestAuthController extends Controller
{
    public function login()
    {
        if (Auth::attempt(request()->only('email', 'password'))) {
            Log::debug("--- authorized ---");
            return 'Authorized.';
        }

        Log::debug("--- not authorized ---");
        return 'Not Authorized.';

    }
}